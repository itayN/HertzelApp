package com.example.itay.hertzelapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    //TODO connect this project to fireBase
    TextView hertzelTv;
    SeekBar numberOfLSeekBar;
    Button sendBtn;
    int numberOfL;//the number of L in the hertzelTv
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeVariables();
        seekBarListener();
        banListener();
    }
    public void initializeVariables(){
        hertzelTv=findViewById(R.id.hertzel_tv);
        numberOfLSeekBar=findViewById(R.id.number_L_seekBar);
        sendBtn=findViewById(R.id.send_hertzel_btn);
    }

    public void banListener(){
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open dialog to choose who to send it to
                //TODO creat a dialog that will open in this click. the dialog will be the GUI for sending a message.
            }
        });
    }

    public void seekBarListener(){
        numberOfLSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress=1;//progress= number of L in the hertzel

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                this.progress=progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                numberOfL=progress;
                setNewTvValue(numberOfL);
            }
        });
    }

    public void setNewTvValue(int numberOfL){
        String newTv=getResources().getString(R.string.hertzel_tv);
        for(int i=1;i<numberOfL;i++){//i=1 because hertzel is always with at least one L
            newTv=newTv+"ל";
        }
        hertzelTv.setText(newTv);
    }
}